package com.example.examengapsi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.examengapsi.retrofit.response.Item;

import java.util.List;

public class GapsiRecyclerViewAdapter extends RecyclerView.Adapter<GapsiRecyclerViewAdapter.ViewHolder>{

    private List<Item> mValues;
    private Context context;


    public GapsiRecyclerViewAdapter(Context context, java.util.List<Item> items) {
        mValues = items;
        this.context= context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_producto, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.titulo.setText(holder.mItem.getTitle());
        holder.precio.setText("$ "+ holder.mItem.getPrice());
        Glide.with(context).load(holder.mItem.getImage()).into(holder.producto);

    }

    @Override
    public int getItemCount() {
        if(mValues != null){
            return mValues.size();
        }else{
            return 0;
        }
    }

    public void getActualice(List<Item> items) {
        this.mValues=items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView titulo;
        public final TextView precio;
        public final ImageView producto;
        public Item mItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView=itemView;
            titulo=itemView.findViewById(R.id.tv_titulo);
            precio=itemView.findViewById(R.id.tv_precio);
            producto=itemView.findViewById(R.id.iv_imagen);
        }
    }
}
