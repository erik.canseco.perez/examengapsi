package com.example.examengapsi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examengapsi.common.MyApp;
import com.example.examengapsi.data.SearchViewModel;
import com.example.examengapsi.retrofit.response.Item;
import com.example.examengapsi.retrofit.response.ProductosEntity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SearchView search;
    private String buscar;
    private GapsiRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private SearchViewModel viewModel;
    private List<Item> productos = new ArrayList<Item>();
    private LiveData<ProductosEntity> listproducto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        search = findViewById(R.id.sv_search);
        recyclerView = findViewById(R.id.rv_producto);
        viewModel =  new ViewModelProvider(this).get(SearchViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GapsiRecyclerViewAdapter(this,productos);

        recyclerView.setAdapter(adapter);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                buscar=query;
                getData(query);
                search.setQuery("", false);
                search.setIconified(true);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

    }

    private void getData(String query) {
        viewModel.getProductos(query).observe(this, new Observer<ProductosEntity>() {
            @Override
            public void onChanged(ProductosEntity productosEntity) {
                productos=productosEntity.getItems();
                adapter.getActualice(productosEntity.getItems());
            }
        });
    }


}