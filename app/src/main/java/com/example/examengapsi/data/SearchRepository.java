package com.example.examengapsi.data;



import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;


import com.example.examengapsi.common.MyApp;
import com.example.examengapsi.retrofit.GapsiCliente;
import com.example.examengapsi.retrofit.GapsiServices;
import com.example.examengapsi.retrofit.response.ProductosEntity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchRepository {
    GapsiServices gapsiServices;
    GapsiCliente gapsiCliente;
    MutableLiveData<ProductosEntity> productos;
    

    public SearchRepository() {
        gapsiCliente =gapsiCliente.getInstance();
        gapsiServices=gapsiCliente.getGapsiServices();
    }

    public MutableLiveData<ProductosEntity> getProductos(String query) {

        if(productos== null){
            productos = new MutableLiveData<>();
        }

        Call<ProductosEntity> call = gapsiServices.getsearch(query);
        call.enqueue(new Callback<ProductosEntity>() {
            @Override
            public void onResponse(Call<ProductosEntity> call, Response<ProductosEntity> response) {
                if (response.isSuccessful()){
                    productos.setValue(response.body());
                    Log.e("datos Repositorio",response.body().toString());
                }else{
                    Toast.makeText(MyApp.getContext(),"Algo ha ido mal",Toast.LENGTH_LONG).show();
                    Log.e("Algo salio mal  ","respose");
                }
            }

            @Override
            public void onFailure(Call<ProductosEntity> call, Throwable t) {
                Log.e("datos Repositorio","Algo ha ido mal"+ t.getMessage());
                Toast.makeText(MyApp.getContext(),"Algo ha ido mal"+ t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


        return productos;
    }


}
