package com.example.examengapsi.data;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.examengapsi.retrofit.response.ProductosEntity;

public class SearchViewModel extends AndroidViewModel {
    private SearchRepository searchRepository;
    private LiveData<ProductosEntity> productos;


    public SearchViewModel(@NonNull Application application) {
        super(application);
        searchRepository = new SearchRepository();
    }

    public LiveData<ProductosEntity> getProductos(String search){

        productos = searchRepository.getProductos(search);
        return productos;
    }
}
