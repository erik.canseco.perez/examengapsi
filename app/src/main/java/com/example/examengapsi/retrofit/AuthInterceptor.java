package com.example.examengapsi.retrofit;

import com.example.examengapsi.common.Constantes;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request().newBuilder().addHeader(Constantes.API_KEY_PARAMETROS,Constantes.API_KEY).build();
        return chain.proceed(request);
    }
}
