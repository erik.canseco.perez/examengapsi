package com.example.examengapsi.retrofit;

import android.util.Log;

import com.example.examengapsi.common.Constantes;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GapsiCliente {
    private static GapsiCliente instance= null;
    private GapsiServices gapsiServices;
    private Retrofit retrofit;

    public GapsiCliente() {
        OkHttpClient.Builder okHttpClientBuilder= new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        OkHttpClient cliente = okHttpClientBuilder.build();

        retrofit=new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(cliente)
                .build();

        gapsiServices=retrofit.create(GapsiServices.class);

    }

    public static GapsiCliente getInstance(){
        if(instance == null){
            instance = new GapsiCliente();
        }
        return instance;
    }

    public  GapsiServices getGapsiServices(){
        return gapsiServices;
    }
}
