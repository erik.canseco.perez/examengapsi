package com.example.examengapsi.retrofit;

import com.example.examengapsi.retrofit.response.ProductosEntity;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GapsiServices {
    @GET("search")
    Call<ProductosEntity> getsearch(@Query("query") String query);
}
