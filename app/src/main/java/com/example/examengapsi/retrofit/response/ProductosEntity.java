
package com.example.examengapsi.retrofit.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductosEntity {

    @SerializedName("totalResults")
    @Expose
    private int totalResults;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ProductosEntity() {
    }

    /**
     * 
     * @param totalResults
     * @param page
     * @param items
     */
    public ProductosEntity(int totalResults, int page, List<Item> items) {
        super();
        this.totalResults = totalResults;
        this.page = page;
        this.items = items;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
